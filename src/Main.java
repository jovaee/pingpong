import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Main extends JFrame implements ActionListener {

    // Should a gameloop be used or will this just be a event driven application
    static final boolean USE_GAME_LOOP = true;
    // Should a FPS counter be shown
    static final boolean SHOW_FPS = true;
    // More or less the time between each redraw of the window in milliseconds. 33 = 30FPS; 16 = 60FPS
    static final int FRAMETIME = 16;

    // Window settings
    static final String WINDOW_TITLE = "PingPong";
    static final int WINDOW_HEIGHT = 500;
    static final int WINDOW_WIDTH = 500;

    // Game state stuff
    static final int PADDLE_SPEED = 10;

    private int leftPaddlePos = 0;
    private int leftPaddleSpeed = 0;
    private int rightPaddlePos = 0;
    private int rightPaddleSpeed = 0;

    private int ballXPos = WINDOW_WIDTH / 2;
    private int ballYPos = WINDOW_HEIGHT / 2;
    private double ballXSpeed = -1.7;
    private double ballYSpeed = 1.1;

    /**
     * Main
     */
    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        EventQueue.invokeLater(Main::new);
    }

    public Main() {
        init();
    }

    private void init() {
        this.setTitle(WINDOW_TITLE);
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        DrawingCanvas dc = new DrawingCanvas();
//        dc.addMouseMotionListener(new MouseMotionListener() {
//            @Override
//            public void mouseDragged(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseMoved(MouseEvent e) {
//                System.out.println("X: " + e.getX() + " Y: " + e.getY());
//            }
//        });

        // Finally add the canvas that everything is drawn onto
        this.add(dc);

        // Add a keyboard listener to the JFrame
        this.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_DOWN:
                        leftPaddleSpeed = PADDLE_SPEED;
                        break;
                    case KeyEvent.VK_UP:
                        leftPaddleSpeed = -PADDLE_SPEED;
                        break;
                    case KeyEvent.VK_S:
                        rightPaddleSpeed = PADDLE_SPEED;
                        break;
                    case KeyEvent.VK_W:
                        rightPaddleSpeed = -PADDLE_SPEED;
                        break;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_DOWN:
                    case KeyEvent.VK_UP:
                        leftPaddleSpeed = 0;
                        break;
                    case KeyEvent.VK_W:
                    case KeyEvent.VK_S:
                        rightPaddleSpeed = 0;
                        break;
                }
            }
        });

        if (USE_GAME_LOOP) {
            // Start a timer that will act as the game loop. Any update logic should go in here
            new Timer(FRAMETIME, this).start();
        }

        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // Called by the timer/gameloop. All update logic should go in here that requires
        // some king of update every frame ie animations etc

        // Set the paddle bounds
        if (leftPaddlePos + leftPaddleSpeed >= 0 && leftPaddlePos + leftPaddleSpeed <= WINDOW_HEIGHT - 41) {
            leftPaddlePos += leftPaddleSpeed;
        }

        if (rightPaddlePos + rightPaddleSpeed >= 0 && rightPaddlePos + rightPaddleSpeed <= WINDOW_HEIGHT - 41) {
            rightPaddlePos += rightPaddleSpeed;
        }

        // Check if ball is touching a paddle
        // TODO: Bounce back ball if touching

        // Check if ball behind paddle
        // TODO If behind give point to winning paddle and reset

        // Check if ball is touch sides and reflect if so
        if (ballXPos + ballXSpeed <= 0 || ballXPos + ballXSpeed + 10 >= WINDOW_WIDTH) {
            ballXSpeed = -ballXSpeed;
            ballXSpeed = ballXSpeed < 0 ? ballXSpeed - 0.35 : ballXSpeed + 0.35;
        }
        if (ballYPos + ballYSpeed <= 0 || ballYPos + ballYSpeed + 10 >= WINDOW_HEIGHT) {
            ballYSpeed = -ballYSpeed;
            ballYSpeed = ballYSpeed < 0 ? ballYSpeed - 0.35 : ballYSpeed + 0.35;
        }

        ballXPos += ballXSpeed;
        ballYPos += ballYSpeed;

        // Repaint the window after all the state updates
        repaint();
    }

    private class DrawingCanvas extends JPanel {

        private long lastTime = System.currentTimeMillis();

        @Override
        public void paint(Graphics g) {
            super.paint(g);

            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            setBackground(Color.WHITE);

            int windowWidth = this.getWidth();
            int windowHeight = this.getHeight();

            // Calculate the scale to draw at.
            // This allow everything to scale nicely as the window is resized
            double xScale = windowWidth / (double) WINDOW_WIDTH;
            double yScale = windowHeight / (double) WINDOW_HEIGHT;

            g2.scale(xScale, yScale);

            if (SHOW_FPS) {
                // Draw some text
                double fps = 1000.0 / (System.currentTimeMillis() - lastTime);

                Color currentColor = g2.getColor();
                Color fpsColor = fps < 30.0 ? Color.RED : Color.GREEN;

                g2.setColor(fpsColor);
                g2.drawString("FPS: " + Double.toString(Math.round(fps * 100.0) / 100.0), 1, 11);
                g2.setColor(currentColor); // Set color back to what it was before drawing FPS counter

                this.lastTime = System.currentTimeMillis();
            }

            // Draw the paddles
            g2.drawRect(15, leftPaddlePos, 5, 45);
            g2.drawRect(WINDOW_WIDTH - 20, rightPaddlePos, 5, 45);

            // Draw the ball
            g2.drawRoundRect(ballXPos, ballYPos, 10, 10, 10, 10);
        }
    }
}